#!/usr/bin/env python

"""
@brief: Recognise named entities from tweet contents and normalize it
@author: Remy Decoupes
@copyright: CeCILL-B

1. Connect mood-tweet-collect Elasticsearch server
2. Loop on each original tweets (i.e. not working on RT)
3. Apply a NER pipeline
4. Normalize spatial entities with openstreetmap data
5. Create a new index and upload data enlarged in elastich search
"""

import eland as ed
import spacy
import requests
import rubrix as rb
from transformers import pipeline
from tqdm import tqdm
from timexy import Timexy  # https://github.com/paulrinckens/timexy

if __name__ == '__main__':
    # Corpus size: nb of tweets to process
    nb_tweets = 10
    # Do you want to log data in rubrix ?
    rubrix = True
    if rubrix == True:
        rb.init(api_url="http://mo-mood-tetis-rubrix.montpellier.irstea.priv:6900")
        rubrix_records = []
    # Configure NLP model
    nlp_en = spacy.load("en_core_web_trf")
    model_checkpoint_fr = "Helsinki-NLP/opus-mt-fr-en"
    translator_fr = pipeline("translation", model=model_checkpoint_fr)
    model_checkpoint_it = "Helsinki-NLP/opus-mt-it-en"
    translator_it = pipeline("translation", model=model_checkpoint_it)
    # Configure timexy type
    config = {
        "kb_id_type": "timex3",  # possible values: 'timex3'(default), 'timestamp'
        "label": "timexy",  # default: 'timexy'
        "overwrite": False  # default: False
    }
    nlp_en.add_pipe("timexy", config=config, before="ner")

    is_SNE = lambda x: True if (x.label_ == "LOC" or x.label_ == "GPE" or x.label_ == "FAC") else False
    is_TNE = lambda x: True if x.label_ == "DATE" else False
    # Configure geocoding
    url_photon = "https://photon.komoot.io/api/?q="
    """
    /!\ This is an inscure script. In a stagging mode
    We are working only on the 10 first tweets. For production, you'll need to remove .head(10).
    """
    # configure connection to mood elasticsearch
    ed_tweets = ed.DataFrame("http://mo-mood-tetis-tweets-collect.montpellier.irstea.priv:9200",
                             es_index_pattern="mood-tetis-tweets-collect")
    # work on original tweet
    for i, tweet in tqdm(ed_tweets[ed_tweets["retweeted_status.id"].isna()].head(nb_tweets).iterrows(),
                         total=nb_tweets):
        # Check lang and apply NLP pipeline
        if tweet["lang"] == "fr":
            text_en = translator_fr(tweet["text"])[0]["translation_text"]
        elif tweet["lang"] == "it":
            text_en = translator_it(tweet["text"])[0]["translation_text"]
        else:
            text_en = tweet["text"]
        ner = nlp_en(text_en)

        list_of_normalize_SNE = []
        list_of_normalize_TNE = []
        SNE_found = False
        TNE_found = False
        if rubrix == True:
            rubrix_tweet_records = []
        for entity in ner.ents:
            if is_SNE(entity):
                SNE_found = True
                # reponses = requests.get(url_photon + str(entity))
                # try:
                # list_of_normalize_SNE.append(reponses.json()['features'][0])
                # except:#when the NER pipeline had wrong label a SNE
                # pass
                if rubrix == True:
                    rubrix_tweet_records.append((entity.label_, entity.start_char, entity.end_char))
            if is_TNE(entity):
                TNE_found = True
                if rubrix == True:
                    rubrix_tweet_records.append((entity.label_, entity.start_char, entity.end_char))
        if rubrix == True and (SNE_found == True or TNE_found == True):  # log in rubrix only if we found a SNE or TNE
            tokens = [token.text for token in ner]
            rubrix_records.append(
                rb.TokenClassificationRecord(
                    text=text_en + "||| translate from: " + tweet["text"],
                    tokens=tokens,
                    prediction=rubrix_tweet_records,
                    prediction_agent="geocode_tweet_content_translation_with_tne"
                )
            )

    if rubrix == True:
        rb.log(records=rubrix_records, name="geocode_tweet_content_translation_with_tne")
