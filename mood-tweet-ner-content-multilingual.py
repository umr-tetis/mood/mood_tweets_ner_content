#!/usr/bin/env python

"""
@brief: Recognise named entities from tweet contents and normalize it
@author: Remy Decoupes
@copyright: CeCILL-B

1. Connect mood-tweet-collect Elasticsearch server
2. Loop on each original tweets (i.e. not working on RT)
3. Apply a NER pipeline
4. Normalize spatial entities with openstreetmap data
5. Create a new index and upload data enlarged in elastich search
"""

import eland as ed
import spacy
import requests
import rubrix as rb

if __name__ == '__main__':
    # Do you want to log data in rubrix ?
    rubrix = True
    if rubrix == True:
        rb.init(api_url="http://mo-mood-tetis-rubrix.montpellier.irstea.priv:6900")
        rubrix_records = []
    # Configure NLP model
    nlp_en = spacy.load("en_core_web_trf")
    nlp_fr = spacy.load("fr_core_news_lg")
    nlp_it = spacy.load("it_core_news_lg")
    nlp_multi = spacy.load("xx_ent_wiki_sm")
    is_SNE = lambda x: True if (x.label_ == "LOC" or x.label_ == "GPE" or x.label_ == "FAC") else False
    # Configure geocoding
    url_photon = "https://photon.komoot.io/api/?q="
    """
    /!\ This is an inscure script. In a stagging mode
    We are working only on the 10 first tweets. For production, you'll need to remove .head(10).
    """
    # configure connection to mood elasticsearch
    ed_tweets = ed.DataFrame("http://mo-mood-tetis-tweets-collect.montpellier.irstea.priv:9200",
                            es_index_pattern="mood-tetis-tweets-collect")
    # work on original tweet
    for i, tweet in ed_tweets[ed_tweets["retweeted_status.id"].isna()].head(10).iterrows():
        # Check lang and apply NLP pipeline
        if tweet["lang"] == "en":
            ner = nlp_en(tweet["text"])
        elif tweet["lang"] == "fr":
            ner = nlp_fr(tweet["text"])
        elif tweet["lang"] == "it":
            ner = nlp_it(tweet["text"])
        else:
            ner = nlp_multi(tweet["text"])

        list_of_normalize_SNE = []
        SNE_found = False
        if rubrix == True:
            rubrix_tweet_records = []
        for entity in ner.ents:
            if is_SNE(entity):
                # print(tweet["text"])
                # print(tweet["lang"])
                # print(entity)
                # print(entity.label_)
                SNE_found = True
                reponses = requests.get(url_photon + str(entity))
                try:
                    list_of_normalize_SNE.append(reponses.json()['features'][0])
                except:#when the NER pipeline had wrong label a SNE
                    pass
                if rubrix == True:
                    rubrix_tweet_records.append((entity.label_, entity.start_char, entity.end_char))
        if rubrix == True and SNE_found == True: #log in rubrix only if we found a SNE
            tokens = [token.text for token in ner]
            rubrix_records.append(
                rb.TokenClassificationRecord(
                    text=tweet["text"],
                    tokens=tokens,
                    prediction=rubrix_tweet_records,
                    prediction_agent="geocode_tweet_content"
                )
            )

    if rubrix == True:
        rb.log(records=rubrix_records, name="geocode_tweet_content")

