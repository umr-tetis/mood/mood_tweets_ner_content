# Named Entities Recognition inside tweets content
Extraction spatial and tamporal named entities from tweet contents
![img.png](readme_ressources/displacy_ner.png)

![img.png](readme_ressources/photon_example.png)
## Install this repository
+ Install pip libraries : `pip3 install ./requirements.txt`
+ Install a spacy model for multi-language Named Entities: **[xx_ent_wiki_sm](https://spacy.io/models/xx#xx_ent_wiki_sm)**: 
```
python3 -m spacy download xx_ent_wiki_sm
python3 -m spacy download en_core_web_trf
python3 -m spacy download fr_core_news_lg
python3 -m spacy download it_core_news_lg
```
  + Comparing F-SCORE for NER taskd between models :
    + [xx_ent_wiki_sm](https://spacy.io/models/xx#xx_ent_wiki_sm): 0,84
    + The SOTA for English [en_core_web_trf](https://spacy.io/models/en#en_core_web_trf): 0,90
    + The SOTA for French [fr_core_news_lg](https://spacy.io/models/fr#fr_core_news_lg): 0,84
    + The SOTA for Italian [it_core_news_lg](https://spacy.io/models/it#it_core_news_lg): 0,88
## Launch scripts
This repository contains 2 kinds of scripts: notebook and traditionnal python scripts
### Jupyter notebook
You can start a jupyterlab : ```jupyter lab```
But if you prefer old notebook : ```jupyter notebook```
## Librairies used
+ Spacy
+ timexy: detect time entities (duration and time)
+ BertViz
+ ![img.png](readme_ressources/bertviz.png)
## License
This code is provided under the [CeCILL-B](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) free software license agreement.
